'use strict';

if (typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(['log4js'], function (log4js) {

    /**
     * @description Configurações do sistema de log
     */
    log4js.configure({
        "appenders": [
            {
                type: "console"
            },

            {
                "type": "dateFile",
                "filename": __dirname + "/access.log",
                "pattern": "-yyyy-MM-dd",
                "category": "ALL"
            },

            {
                "type": "file",
                "filename": __dirname + "/app.log",
                "maxLogSize": 10485760,
                "numBackups": 3
            },

            {
                "type": "logLevelFilter",
                "level": "ERROR",
                "appender": {
                    "type": "file",
                    "filename": __dirname + "/errors.log"
                }
            }
        ],
        replaceConsole: true
    });

    /**
     * Seta log como modo geral
     * @type {Logger}
     */
    log4js.getLogger("ALL");

});