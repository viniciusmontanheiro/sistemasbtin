'use strict';

if (typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(['pg','../../commons/dao/postgresDao.js'],
    function (Postgres, PostgresDao) {

        var status = {
            dao: '',
            connected: false
        };

        /**
         * @description Gerênciador de conexões com postgres
         * @param config
         * @returns {{dao: string, connected: boolean}}
         * @constructor
         */
        exports.connector = function PostgresConnector(config) {

            console.info("Iniciando conexão com postgres ...");

            //String de conexao ao banco de dados
            var uri = "postgres://"
                + config.user + ":"
                + config.pass + "@"
                + config.host + ":"
                + config.port + "/"
                + config.name;

            var client = new Postgres.Client(uri);

            //Realiza a conexão
            client.connect(function (err) {
                if (err) {
                    console.error(new Error('Tentativa de conexão com postgres falhou! \n', err));
                }
                else {
                    console.info("Postgres connectado em " + config.name);
                    status.client = client;
                    status.dao = new PostgresDao(client);
                    status.connected = true;
                }
            });

            process.on("SIGINT", function () {
                client.end(function () {
                    console.info("Postgres! Desconnectado pelo término da aplicação.");
                    process.exit(0);
                });
            });

            return status;
        };

        exports.query = function () {
            return status.connection.query;
        };
    });


