if (typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(['passport-local','passport-facebook','password-generator','../oAuth/auth.js', '../../app/models/user.js'],

    function (passportLocal,passportFacebook,generatePassword,configAuth, User) {
        var LocalStrategy = passportLocal.Strategy;
        var FacebookStrategy = passportFacebook.Strategy;
        var email = global.mail;

        /**
         * @description Configurações da sessão
         * @required passport, passport-local strategy
         */
        module.exports = function (passport) {

            // Serializa o usuário e o coloca na sessão
            passport.serializeUser(function(user, done) {
                done(null, user.id);
            });
            // De-serializa o usuário
            passport.deserializeUser(function(id, done) {
                User.findById(id, function(err, user) {
                    done(err, user);
                });
            });

            // LOCAL SIGNUP
            passport.use('local-signup', new LocalStrategy({
                    usernameField: 'login',
                    passwordField: 'password',
                    passReqToCallback: true // Permite passar as solicitações por callback
                },
                function (req, login, password, done) {

                    process.nextTick(function () {

                        //Verifica se o cpf é o mesmo do formulário enviado
                        User.findOne({'local.login': login}, function (err, user) {
                            if (err) {
                                console.error(new Error("Erro ao buscar usuário!!"), err);
                                return done(err);
                            }

                            // Verifica se o email informado já existe
                            if (user) {
                                return done(null, false, console.error(new Error('O usuário informado já existe.')));
                            } else {

                                // Se não possuir o usuário, nós criamos!
                                var newUser = new User();

                                // Colocando as credencias do usuário
                                newUser.local.login = login;
                                newUser.local.password = newUser.generateHash(password);

                                // Salvando
                                newUser.save(function (err) {
                                    if (err) {
                                        throw err;
                                    }
                                    return done(null, newUser);
                                });
                            }

                        });

                    });

                }));

            passport.use('local-login', new LocalStrategy({
                    // Por padrão a estratégia local usa nome de usuário e senha, nós vamos re-escrever com cpf
                    usernameField: 'login',
                    passwordField: 'password',
                    passReqToCallback: true // Permite passar as solicitações por callback
                },
                function (req, login, password, done) { // callback com cpf e password do formulário

                    // Verificando se o login enviado pelo formulário está cadastrado
                    User.findOne({'local.login': login}, function (err, user) {

                        if (err) {
                            console.error(new Error('Erro ao buscar usuário.'), err);
                            return done(err);
                        }

                        // Se o usuário não existir
                        if (!user) {
                            return done(null, false, console.error(new Error('Usuário não encontrado!!')));
                        }

                        // Se o usuário for encontrado mas o password estiver errado
                        if (!user.validPassword(password)) {
                            return done(null, false, console.error(new Error('Senha inválida!'), err));
                        }
                        console.log(user);
                        // Se tudo estiver ok, autorizamos o usuário
                        return done(null, user);
                    });

                }));


            /**
             * @Description Configurações de login com sincronização do Facebook.
             */
            passport.use(new FacebookStrategy({

                    // Pegando as configurações do app
                    clientID        : configAuth.facebookAuth.clientID,
                    clientSecret    : configAuth.facebookAuth.clientSecret,
                    callbackURL     : configAuth.facebookAuth.callbackURL

                },
                // Passando por callback o midleware e os dados do facebook
                function(req, token, refreshToken, profile, done) {

                    // asynchronous
                    process.nextTick(function() {

                        // Verificando se o usuário já está na sessão
                        if (!req.user) {

                            // Busca o usuário no banco pelo seu id
                            User.findOne({ 'facebook.id' : profile.id }, function(err, user) {

                                if (err) {
                                    console.error(new Error('Erro ao buscar usuário pelo Facebook!'),err);
                                    return done(err);
                                }

                                // Se o usuário for encontrado
                                if (user) {

                                    // Se o usuário estiver logado, mas, não possuir token, nós devemos definir ele
                                    if (!user.facebook.token) {
                                        user.facebook.token = token;
                                        user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                                        user.facebook.email = profile.emails[0].value;

                                        user.save(function(err) {
                                            if (err) {
                                                console.log(new Error('Erro ao sincronizar dados do Facebook!!'));
                                                throw err;
                                            }
                                            return done(null, user);
                                        });
                                    }
                                    return done(null, user);
                                } else {
                                    // Se o usuário não for encontrado com esse Id, nós devemos criar ele
                                    var newUser = new User();

                                    // Definindo todas as informações do Facebook
                                    newUser.facebook.id    = profile.id; // Id
                                    newUser.facebook.token = token; // Token
                                    newUser.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName; // Variedades de nomes retornados pelo passport
                                    newUser.facebook.email = profile.emails[0].value; // Pegando apenas um dos emails retornados

                                    newUser.save(function(err) {
                                        if (err) {
                                            console.log(new Error('Erro ao sincronizar dados do Facebook!!'));
                                            throw err;
                                        }
                                        // Vinculando contra de usuário local com os dados do Facebook.
                                        newUser.local.email = profile.emails[0].value;
                                        newUser.local.password = newUser.generateHash(generatePassword(16, false));
                                        var headers = {
                                            from : 'arquitetura@arquitetura.com',
                                            to : newUser.local.email,
                                            subject : 'Arquitetura local access password',
                                            html : '<i> Your local password from arquitetura is </i> <b>'
                                            + newUser.local.password
                                            + '</b></br> <a href="link/changePassword">Clique aqui para alterar sua senha!</a>'
                                        };

                                        email.send(headers);
                                        return done(null, newUser);
                                    });
                                }

                            });

                        } else {
                            // O usuário está logado, então devemos sincronizar os dados
                            var user = req.user;

                            // Atualizando usuário com suas credênciais
                            user.facebook.id    = profile.id;
                            user.facebook.token = token;
                            user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                            user.facebook.email = profile.emails[0].value;

                            user.save(function(err) {
                                if (err) {
                                    console.log(new Error('Erro ao sincronizar dados do Facebook!!'));
                                    throw err;
                                }
                                return done(null, user);
                            });
                        }

                    });

                }));
        };

    });




