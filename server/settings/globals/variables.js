'use strict';

/**
 * @desc Unifica as configurações globais da aplicação
 * @author Vinícius Montanheiro
 */


//Classe de utilitátios
var Util = require('../../commons/utils/util');
global.util = new Util();

//Classe de validações
var Validation = require('../../commons/utils/validation');
global.validation = new Validation();

//Define logs e mensagens do sistema
require('../logs/logger');

//Criação de eventos genéricos
var Events = require('events');
global.event = new Events.EventEmitter();

//Envio de emails
var Mail = require('../../commons/mail/mailer');
global.mail = new Mail();

//Faz conexão com os bancos disponíveis
var FactoryDatabases = require('../../commons/dao/factoryDatabases');
var configs = require("../../configs.json");
global.factory = new FactoryDatabases().getConnection(configs.databases);
global.postgres = global.factory.connections[1];








