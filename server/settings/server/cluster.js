'use strict';

/**
 * Created by vinicius on 06/04/15.
 */

//A bit of boilerplate to resolve the AMD modules
if (typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(['cluster', 'util', 'os'], function (cluster, util, os) {
    var numCPUs = os.cpus().length;
    function Clustering() {

        this.init = function () {
            //Verificando se o processo pai é único
            if (cluster.isMaster) {
                console.info('Aplicação iniciada com %d clusters', numCPUs);

                //Criando os filhos baseando no número de CPUS
                for (var i = 0; i < numCPUs; i++) {
                    cluster.fork();
                }

                cluster.on('online', function (workers) {
                    console.info("Instância[" + workers.id + "]" + ": PID[" + workers.process.pid + "] - " + workers.state);
                });

                cluster.on('exit', function (worker, code, signal) {
                    //Se fizermos kill em algum dos filhos
                    if (worker.suicide === true) {
                        console.warn('Oh, isso foi suicídio!\' – não se preocupe.');
                        //Se não
                    } else {
                        console.info('Processo %d morto (%s)', worker);
                    }
                });

                cluster.on('message', function (msg) {
                    console.info("Mensagens para os filhos: (%s)" + msg);
                    if (msg == "exit") {
                        this.killAll();
                    }
                });

            } else {
                //Compartilhando o servidor entre os filhos
                Server();
            }
        };

        this.killAll = function () {
            for (var id in cluster.workers) {
                cluster.workers[id].kill(id);
            }
            process.exit(0);
        };

        this.getMemoryInfo = function () {
            console.info(util.inspect(process.memoryUsage()));
        }
    };
    module.exports = Clustering;
});




