'use strict';

/**
 * @description Configurações do servidor
 * @author Vinícius Montanheiro
 */

if (typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define([
          'https'
        , 'http'
        , 'path'
        , 'lodash'
        , 'express'
        , 'method-override'
        , 'body-parser'
        , 'cookie-parser'
        , 'multer'
        , 'passport'
        , 'express-session'
        , 'helmet'
        , './../../app/routes.js'
        , 'fs'
        , 'socket.io'
        , './socket.js'
        ,'../oAuth/passport.js'],

    function (https, http, path, _, express, methodOverride, bodyParser, cookieParser, multer, passport, session, helmet, Routes, fs, socketio,Socket,Auth) {

        var root = path.normalize(__dirname + '/../../..');
        var app = express();
        //var privateKey = fs.readFileSync(__dirname + '/ssl/server.key').toString();
        //var certificate = fs.readFileSync(__dirname + '/ssl/server.crt').toString();
        //var credentials = {key: privateKey, cert: certificate};

        module.exports = function () {

            //Configurações do express
            app.set("port", 8080);
            app.use(cookieParser());
            app.use(bodyParser.urlencoded({extended: true}));
            app.use(bodyParser.json());
            app.use(methodOverride());
            app.use(multer());
            app.set('views', root + '/client/views');
            app.engine('html', require('ejs').renderFile);
            app.set('view engine', 'html');
            app.use(express.static(path.join(root, '/client')));
            app.use(helmet());
            app.use(session({
                secret: 'xsexylog checz',
                resave: true,
                saveUninitialized: true
            }));
            app.use(passport.initialize());
            app.use(passport.session());

            //Configurações de segurança
            app.use(helmet.xframe()); // Nenhum iframe pode referênciar essa aplicação
            app.use(helmet.xssFilter()); // Proteção contra ataques xss
            app.use(helmet.nosniff()); // Não permite que o browser modifique o MIME TYPE
            app.disable('x-powered-by'); // Removendo informações da tecnologia usada
            app.use(helmet.hidePoweredBy({setTo: 'PHP 5.5'})); // Atribui informações falsas ao powered by

            //Rotas
            Routes(app,passport);
            //oAuth
            Auth(passport);

            // Estabelecendo servidor
            var server = http.createServer(app);
            // Configurações do socket
            var io = socketio(server);

            // Iniciando servidor
            server.listen(app.get("port"), function (err) {
                if (err) {
                    console.error(new Error(':-( Server DOWN!!'), err);
                }
                console.info('Servidor iniciado na porta %d', app.get("port"));
                //Instanciando socket
                //Socket(io);
            });
        };
    });









