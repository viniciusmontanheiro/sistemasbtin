'use strict';

/**
 * @description Implementação do index controller
 */

exports.index = function (req, res, next) {
    res.render('index',{
        titulo : "Sistemas BTin",
        descricao : "Gestor de monitoramento."
    });
    next();
};


