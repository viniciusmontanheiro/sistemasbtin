/**
 *@description Implementação do error controller
 * @param req
 * @param res
 * @param next
 */

//Tratamento de página não encontrada
exports.notFound = function (req, res, next) {
    res.render('error/404', {
            lang: 'pt-br'
        });
    next();
};

//Encaminha as conexões da porta 80 para 443
exports.secure = function (req, res, next) {
    if (req.protocol != "https") {
        res.set('x-forwarded-proto', 'https');
        res.redirect('https://' + req.get('host') + req.url);
    } else {
        next();
    }
};