

exports.logging = function(req, res, next) {
    res.render('index',{

        titulo : "Sistemas BTin",
        descricao : "Gestor de monitoramento."
    });
    next();
};

exports.signup = function(req, res,next) {
    res.render('profile/dashboard', {
        login : req.user.login,
        titulo : 'BTin'
    });
    next();
};

exports.profile = function(req, res, next) {
    res.render('profile/dashboard', {
        login : req.user.login,
        titulo : 'BTin'
    });
    next();
};

exports.logout = function(req, res, next) {
    req.logout();
    res.redirect('/');
    next();
};

exports.isLoggedIn = function isLoggedIn(req, res, next) {
    // Se o usuário estiver autenticado, go ahead
    if (req.isAuthenticated()){
        return next();
    }
    res.redirect('/');
};


