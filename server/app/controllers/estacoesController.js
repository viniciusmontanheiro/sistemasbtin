var Ccom = require('../models/ccom');
var Ecom = require('../models/ecom');
var Validator = global.validation;

exports.getEstacoes = function(req, res,next) {
    var client = global.postgres.client;
    var estacao = req.query;

    if (Validator.isEmpty(estacao, ['nome'])) {
        return console.error(new Error("Por favor selecione a estação!"));
    }
    client.query('SELECT * FROM ' + estacao.nome + " order by id desc", function(err, result) {
        if (err) {
            return console.error('error running query', err);
        }
        res.json(result.rows);
    });
};

exports.getTodasEstacoes = function(req, res,next) {
    var client = global.postgres.client;

    client.query('SELECT * FROM ccom order by id asc', function(err, result) {
        var dados = {
            ccom : [],
            ecom :[]
        };

        if (err) {
            return console.error('error running query', err);
        }

        dados.ccom = result.rows;

        client.query('SELECT * FROM ecom order by id asc', function(err, result) {
            if (err) {
                return console.error('error running query', err);
            }

            dados.ecom = result.rows;
            res.json(dados);
        });
    });
};
