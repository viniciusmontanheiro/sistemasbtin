var mongoose = require('mongoose');

var ecomSchema = mongoose.Schema({
    direcaoVento       : String,
    velocidadeVento   : Number,
    nivelChuva       : Number,
    temperaturaAr     : Number,
    umidadeAr       : Number,
    radiacaoSolar    : Number,
    indiceUltravioleta : Number,
    rajadaVento       : Number,
    indiceResfriamento : Number,
    pontoOrvalho       : Date
});

module.exports = mongoose.model('ecom', ecomSchema);