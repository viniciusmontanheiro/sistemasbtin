var mongoose = require('mongoose');

var ccomSchema = mongoose.Schema({

    direcaoVento       : String,
    velocidadeVento    : Number,
    nivelChuva         : Number,
    nivelChuvaPcom     : Number,
    temperaturaAr      : Number,
    umidadeAr          : Number,
    temperaturaSolo    : Number,
    pressaoAtmosferica : Number,
    umidadeSolo        : Number,
    radiacaoSolar      : Number,
    dataAtual          : Date

});

module.exports = mongoose.model('ccom', ccomSchema);
