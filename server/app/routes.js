'use strict';

/**
 * @description Unificação das rotas do sistema.
 * @param app
 */

if (typeof define !== 'function') {
    var define = require('amdefine')(module);
}
define(['./controllers/indexController', './controllers/errorController', './controllers/loginController.js','./controllers/estacoesController.js'],

    function (indexController, errorController, loginController, estacoesController) {

        module.exports = function (app, passport) {

            //INDEX ROUTES
            app.get("/", indexController.index);

            //LOGIN ROUTES
            app.get('/login', loginController.logging);
            //Processa o formulário de login
            app.post('/login', passport.authenticate('local-login', {
                successRedirect : '/profile', // Redireciona para página do usuário
                failureRedirect : '/login' // Redireciona para o login se ocorrer algum erro
            }));
            app.get('/signup', loginController.signup);
            //Processa o formulário de cadastro
            app.post('/signup', passport.authenticate('local-signup', {
                successRedirect : '/profile', // Redireciona para página do usuário
                failureRedirect : '/signup' // Redireciona para o cadastro se ocorrer algum erro
            }));
            app.get('/profile', loginController.isLoggedIn, loginController.profile);

            app.get('/logout', loginController.isLoggedIn, loginController.logout);

            app.get('/profile/estacoes/:nome',loginController.isLoggedIn, estacoesController.getEstacoes);
            app.get('/profile/todasEstacoes',loginController.isLoggedIn, estacoesController.getTodasEstacoes);
            //ISSUE ROUTES
            //app.get("*", errorController.notFound);
        };

    });


