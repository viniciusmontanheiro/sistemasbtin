'use strict';


if (typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(function () {
    /**
     * @desc  Validações
     * @author Vinícius Montanheiro
     * @constructor
     */
    function Validation() {
        /**
         * @desc Valida campos vazios
         * @param objeto,array
         * @returns {boolean}
         */
        this.isEmpty = function isEmpty(obj, arr) {
            if (arr != null && arr != undefined) {
                for (var i = 0; i < arr.length; i++) {
                    if (obj.hasOwnProperty(arr[i])) {
                        if (obj[arr[i]] == "" || obj[arr[i]] == undefined || obj[arr[i]].length == 0) {
                            return true;
                        }
                    } else {
                        return true;
                    }
                }
            }
            return false;
        };

    };
    module.exports = Validation;
});
