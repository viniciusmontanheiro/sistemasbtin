'use strict';

/**
 * Created by vinicius on 01/04/15.
 */

if (typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(['../../settings/database/mongoConnector.js'
        , '../../settings/database/postgresConnector.js'],
    function (Mongo,Postgres) {

        var connectionsList = [
            {id: 'mongo', connector: Mongo.connector},
            {id: 'postgres', connector: Postgres.connector}
        ];

        function factoryDatabases() {

            this.connections = [];

            this.getConnection = function (configs) {
                for (var i = 0; i < configs.length; i++) {
                    for (var j = 0; j < connectionsList.length; j++) {
                        if (connectionsList[j].id == configs[i].id) {
                            this.connections.push(new connectionsList[j].connector(configs[i]));
                        }
                    }
                }
                return this;
            };

            //this.findAll = function (Model, req, res, afterSearch) {
            //    for (var i in this.connections) {
            //        this.connections[i].dao.findAll(Model, req, res, afterSearch);
            //    }
            //};
            //
            //this.save = function (Model, req, res, required, afterSave) {
            //    for (var i in this.connections) {
            //        this.connections[i].dao.save(Model, req, res, required, afterSave);
            //    }
            //};
            //
            //this.update = function (Model, req, res, afterUpdate) {
            //    for (var i in this.connections) {
            //        this.connections[i].dao.update(Model, req, res, afterUpdate);
            //    }
            //};
            //
            //this.delete = function (Model, req, res, afterDelete) {
            //    for (var i in this.connections) {
            //        this.connections[i].dao.delete(Model, req, res, afterDelete);
            //    }
            //};

        };

        module.exports = factoryDatabases;
    });


