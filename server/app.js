/**
 * Created by vinicius on 07/04/15.
 */
var requirejs = require('requirejs');

requirejs.config({
    //Pass the top-level main.js/index.js require
    //function to requirejs so that node modules
    //are loaded relative to the top-level JS file.
    nodeRequire: require
});

requirejs(['./settings/globals/variables.js','child_process','./configs','./settings/server/cluster.js', './settings/server/server.js'],
    function (variables, childProcess, configs, Cluster, Server) {

        var terminal = childProcess.exec;
        //var command = "grunt mongo";

        //terminal(command, function(err, stdout, stderr) {
        //    if (err) {
        //        console.error(new Error(':-( Falha ao iniciar mongo pelo grunt!'), err);
        //    }

            //Se, inicia aplicação com clusters
            if (configs.clustered) {
                var clustering = new Cluster();
                clustering.init();
            } else {
                Server();
            }
        //});
    });
