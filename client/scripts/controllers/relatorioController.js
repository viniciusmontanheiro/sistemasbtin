"use strict";

define(['angular'], function (angular) {

    var appControllers = angular.module("relatorioController", ['appServices']);

    appControllers.controller('RelatorioController', ['$scope', '$rootScope', 'Util', 'ApiService', function ($scope, $rootScope, Util, ApiService) {
        var util = Util;
        var api = ApiService;
        $rootScope.header.titulo = "Relatórios";

        $scope.estacoes = {
            ecom: {
                colunas: {
                    descricao: [],
                    desc: []
                }
            },
            ccom: {
                colunas: {
                    descricao: [],
                    desc: []
                }
            },
            colunas: {
                ccom: [],
                ecom: []
            },
            isCcom: false,
            isEcom: false,
            isShowGrid : false,
            data: {
                ccom: [],
                ecom: []
            }
        };

        $scope.estacoes.selected = 0;
        $scope.estacoes.fn = {};

        $scope.estacoes.ccom.colunas.descricao.push(buildColumnObj("ID"));
        $scope.estacoes.ccom.colunas.descricao.push(buildColumnObj("Índice de Resfriamento"));
        $scope.estacoes.ccom.colunas.descricao.push(buildColumnObj("Índice Ultravioleta"));
        $scope.estacoes.ccom.colunas.descricao.push(buildColumnObj("Rajada de Vento"));
        $scope.estacoes.ccom.colunas.descricao.push(buildColumnObj("Ponto de Orvalho"));
        $scope.estacoes.ccom.colunas.descricao.push(buildColumnObj("Radiação Solar"));
        $scope.estacoes.ccom.colunas.descricao.push(buildColumnObj("Nível de Chuva"));
        $scope.estacoes.ccom.colunas.descricao.push(buildColumnObj("Direção do Vento"));
        $scope.estacoes.ccom.colunas.descricao.push(buildColumnObj("Velocidade do Vento"));
        $scope.estacoes.ccom.colunas.descricao.push(buildColumnObj("Temperatura do Ar"));
        $scope.estacoes.ccom.colunas.descricao.push(buildColumnObj("Umidade do Ar"));
        $scope.estacoes.ccom.colunas.descricao.push(buildColumnObj("Data/Horas"));

        $scope.estacoes.ccom.colunas.desc.push(buildColumnObj("I.RESF"));
        $scope.estacoes.ccom.colunas.desc.push(buildColumnObj("I.UV"));
        $scope.estacoes.ccom.colunas.desc.push(buildColumnObj("R.VENTO"));
        $scope.estacoes.ccom.colunas.desc.push(buildColumnObj("P.ORVALHO"));
        $scope.estacoes.ccom.colunas.desc.push(buildColumnObj("R.SOLAR"));
        $scope.estacoes.ccom.colunas.desc.push(buildColumnObj("N.CHUVA"));
        $scope.estacoes.ccom.colunas.desc.push(buildColumnObj("D.VENTO"));
        $scope.estacoes.ccom.colunas.desc.push(buildColumnObj("V.VENTO"));
        $scope.estacoes.ccom.colunas.desc.push(buildColumnObj("T.AR"));
        $scope.estacoes.ccom.colunas.desc.push(buildColumnObj("U.AR"));
        $scope.estacoes.ccom.colunas.desc.push(buildColumnObj("D/H"));

        $scope.estacoes.ecom.colunas.descricao.push(buildColumnObj("ID"));
        $scope.estacoes.ecom.colunas.descricao.push(buildColumnObj("Pressão Atmosférica"));
        $scope.estacoes.ecom.colunas.descricao.push(buildColumnObj("Umidade do Solo"));
        $scope.estacoes.ecom.colunas.descricao.push(buildColumnObj("Temperatura do Solo"));
        $scope.estacoes.ecom.colunas.descricao.push(buildColumnObj("Nível de Chuva Pcom"));
        $scope.estacoes.ecom.colunas.descricao.push(buildColumnObj("Nível de Chuva"));
        $scope.estacoes.ecom.colunas.descricao.push(buildColumnObj("Direção do Vento"));
        $scope.estacoes.ecom.colunas.descricao.push(buildColumnObj("Velocidade do Vento"));
        $scope.estacoes.ecom.colunas.descricao.push(buildColumnObj("Radiação Solar"));
        $scope.estacoes.ecom.colunas.descricao.push(buildColumnObj("Temperatura Ar"));
        $scope.estacoes.ecom.colunas.descricao.push(buildColumnObj("Umidade Ar"));
        $scope.estacoes.ecom.colunas.descricao.push(buildColumnObj("Data/Horas"));

        $scope.estacoes.ecom.colunas.desc.push(buildColumnObj("P.Atmosférica"));
        $scope.estacoes.ecom.colunas.desc.push(buildColumnObj("U.SOLO"));
        $scope.estacoes.ecom.colunas.desc.push(buildColumnObj("T.SOLO"));
        $scope.estacoes.ecom.colunas.desc.push(buildColumnObj("N.ChuvaPcom"));
        $scope.estacoes.ecom.colunas.desc.push(buildColumnObj("N.Chuva"));
        $scope.estacoes.ecom.colunas.desc.push(buildColumnObj("D.Vento"));
        $scope.estacoes.ecom.colunas.desc.push(buildColumnObj("R.SOLAR"));
        $scope.estacoes.ecom.colunas.desc.push(buildColumnObj("V.VENTO"));
        $scope.estacoes.ecom.colunas.desc.push(buildColumnObj("T.AR"));
        $scope.estacoes.ecom.colunas.desc.push(buildColumnObj("U.AR"));
        $scope.estacoes.ecom.colunas.desc.push(buildColumnObj("D/H"));

        $scope.formatarData = Util.fn.formatarData;


        $scope.estacoes.fn.buscarEstacoes = function buscarTodasEstacoes() {

            var sucesso = function (retorno) {
                $scope.estacoes.colunas.ccom = $scope.estacoes.ccom.colunas.desc;
                $scope.estacoes.data.ccom = retorno.ccom;
                $scope.estacoes.colunas.ecom = $scope.estacoes.ecom.colunas.desc;
                $scope.estacoes.data.ecom = retorno.ecom;
                $scope.estacoes.isShowGrid = true;
            };

            var erro = function (err) {
                console.log(err);
            };

            api.get('/profile/todasEstacoes', sucesso, erro);
        };


        $scope.estacoes.fn.onEstacaoChange = function onEstacaoChange() {
            var estacao = $scope.estacoes.selected;

            var sucesso = function (retorno) {
                if (estacao == "ccom") {
                    $scope.estacoes.colunas.ccom = $scope.estacoes.ccom.colunas.descricao;
                    $scope.estacoes.data.ccom = retorno;
                    $scope.estacoes.isCcom = true;
                    $scope.estacoes.isShowGrid = false;
                } else {
                    $scope.estacoes.colunas.ecom = $scope.estacoes.ecom.colunas.descricao;
                    $scope.estacoes.data.ecom = retorno;
                    $scope.estacoes.isEcom = true;
                    $scope.estacoes.isShowGrid = false;
                }
            };
            var erro = function (err) {
                console.log(err);
            };

            api.getBy('/profile/estacoes/:nome', {
                params: {
                    nome: estacao
                }
            }, sucesso, erro);


        };

        $scope.estacoes.fn.onShowFilter = function(){
            $scope.estacoes.isShowGrid = false;
        };

        function buildColumnObj(titulo) {
            return {
                valor: titulo
            }
        };
    }])
});