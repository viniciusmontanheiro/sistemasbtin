"use strict";
define([
    'angular',
    'scripts/controllers/dashboardController',
    'scripts/controllers/indexController',
    'scripts/controllers/mainController',
    'scripts/controllers/relatorioController'
], function(angular) {

    var appControllers = angular.module("appControllers",[
        'dashboardController',
        'indexController',
        'mainController',
        'relatorioController'
    ]);

});
