"use strict";

define(['angular'], function (angular) {

    var appControllers = angular.module("mainController", []);

    appControllers.controller('MainController', ['$scope','$rootScope', function ($scope,$rootScope) {

        $rootScope.header = {
            titulo :''
        };

    }]);
});

