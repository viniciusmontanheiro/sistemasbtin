"use strict";

define(['angular'], function (angular) {

    var appControllers = angular.module("indexController", ['appServices']);

    appControllers.controller('IndexController', ['$scope', function ($scope) {
        $scope.usuario = {};
    }]);
});
