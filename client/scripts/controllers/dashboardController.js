"use strict";

define(['angular'], function (angular) {

    var appControllers = angular.module("dashboardController", ['appServices']);

    appControllers.controller('DashboardController', ['$scope','$rootScope','Util', function ($scope, $rootScope,Util) {
        var util = Util.fn;
        $scope.tools = {
            icones : []
        };

        $rootScope.header.titulo = "Dashboard";

        //Mapeando os ícones
        $scope.tools.icones.push(util.buildIconsObj('download','../../assets/icons/svg/upload.svg',24));
        $scope.tools.icones.push(util.buildIconsObj('print','../../assets/icons/svg/print.svg',24));



    }]);
});

