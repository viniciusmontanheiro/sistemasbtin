"use strict";
define([
    'angular',
    'scripts/directives/generalDirectives',
    'scripts/directives/formDirectives'

], function(angular) {

    var appDirectives = angular.module("appDirectives",[
        'generalDirectives',
        'formDirectives'
    ]);

});