"use strict";

define(['angular'], function (angular) {

    var appDirectives = angular.module("formDirectives", []);
    /**
     * @desc Máscara para moeda
     * @dependencies Jquery price format
     * @link http://jquerypriceformat.com/
     */
    appDirectives.directive('coMoeda', ['$filter', function ($filter) {

        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                if (!ctrl) return;

                //metodo que formata o valor quando é alterado via controller
                ctrl.$formatters.unshift(function (a) {
                    elem[0].value = ctrl.$modelValue
                    elem.priceFormat({
                        prefix: '',
                        centsSeparator: ',',
                        thousandsSeparator: '.'
                    });
                    return elem[0].value;
                });

                //Metodo que formata o valor quando é alterado via html
                ctrl.$parsers.unshift(function (viewValue) {
                    elem.priceFormat({
                        prefix: '',
                        centsSeparator: ',',
                        thousandsSeparator: '.'
                    });
                    return elem[0].value;
                });

                //Metodo que seta o valor no model
                ctrl.$parsers.push(function (valueFromInput) {
                    console.log(valueFromInput);
                    return parseFloat(valueFromInput.replace(/\.+/g, "").replace(/\,/, '.'));
                });
            }
        };
    }])
    /** mask
     * Ex: <input type='text' mask='9999-9999' placeholder='Telefone'>
     * 9: Qualquer numero;
     * A: Qualquer letra;
     * *: Qualquer numero e letra;
     *
     * @mask : options{
     * 	cep: 99999-999;
     * 	ddd:telefone : (99) 9999-9999;
     *  ddd: (99);
     *  telefone: 9999-9999;
     *  cpf: 999.999.999-99;
     *  cnpj: 99.999.999/9999-99;
     *
     * */
        .directive('mask', function () {
            return {
                restrict: "A",
                link: function (scope, elem, attr, ctrl) {
                    if (attr.mask) {
                        var mask = "";
                        switch (attr.mask) {
                            case 'cep':
                                mask = "99999-999";
                                break;
                            case 'ddd:telefone':
                                mask = "(99) 9999-9999";
                                break;
                            case 'ddd':
                                mask = "(99)";
                                break;
                            case 'telefone':
                                mask = "9999-9999";
                                break;
                            case 'cpf':
                                mask = "999.999.999-99";
                                break;
                            case 'cnpj':
                                mask = "99.999.999/9999-99";
                                break;
                            default:
                                mask = attr.mask;
                        }
                        elem.mask(mask, {placeholder: attr.maskPlaceholder});
                    }
                }
            };
        })

    /** mask-money
     * Ex: <input type='text' mask-money>
     *  money@params: prefix,suffix,affixesStay,thousands,decimal,precision,allowZero,allowNegative;
     *    Ex: <input mask-money="R$" money-prefix="R$" money-thousands=".">
     * */
        .directive('maskMoney', function () {
            return {
                restrict: "A",
                link: function (scope, elem, attr) {
                    elem.maskMoney({
                        prefix: attr.moneyPrefix ? attr.moneyPrefix : (attr.maskMoney ? attr.maskMoney : "R$ "),
                        suffix: attr.moneySuffix,
                        affixesStay: attr.moneyAffixesStay,
                        thousands: attr.moneyThousands ? attr.moneyThousands : ".",
                        decimal: attr.moneyDecimal ? attr.moneyDecimal : ",",
                        precision: attr.moneyPrecision,
                        allowZero: attr.moneyAllowZero,
                        allowNegative: attr.moneyAllowNegative
                    });
                }
            };
        })

    /**
     * @dependencies Requer um ng-model setado no elemento
     * @desc campo que estiver esse parametro aceitara somente numeros
     * */
        .directive('number', function () {
            return {
                require: '?ngModel',
                link: function (scope, element, attrs, ngModelCtrl) {
                    if (!ngModelCtrl) {
                        return;
                    }

                    ngModelCtrl.$parsers.push(function (val) {
                        if (angular.isUndefined(val)) {
                            var val = '';
                        }
                        var clean = val.replace(/[^0-9]+/g, '');
                        if (val !== clean) {
                            ngModelCtrl.$setViewValue(clean);
                            ngModelCtrl.$render();
                        }
                        return clean;
                    });

                    element.bind('keypress', function (event) {
                        if (event.keyCode === 32) {
                            event.preventDefault();
                        }
                    });
                }
            };
        })
    /**
     * @desc máscara e válidação para coordenadas
     */
        .directive('coordenada', ['$filter', function ($filter) {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function (scope, elem, attrs, ctrl) {

                    if (!ctrl) return;

                    ctrl.$formatters.unshift(function (a) {
                        elem[0].value = ctrl.$modelValue;
                        elem.priceFormat({
                            prefix: '',
                            allowNegative: true,
                            thousandsSeparator: '',
                            limit: 17,
                            centsLimit: 15
                        });
                        return elem[0].value;
                    });
                    //metodo que formata o valor quando é alterado via controller

                    //Metodo que formata o valor quando é alterado via html
                    ctrl.$parsers.unshift(function (viewValue) {
                        elem.priceFormat({
                            prefix: '',
                            allowNegative: true,
                            thousandsSeparator: '',
                            limit: 17,
                            centsLimit: 15
                        });
                        return elem[0].value;
                    });

                    //Metodo que seta o valor no model
                    ctrl.$parsers.push(function (valueFromInput) {
                        var coord = valueFromInput.toString();
                        if ((coord != undefined && coord != null && coord.length > 0) && coord.search("-") === -1) {
                            coord = "-" + coord;
                        }
                        return coord;
                    });
                }
            };
        }]).directive('bottomSheet', ['$timeout','$mdBottomSheet', function ($timeout,$mdBottomSheet) {
            return {
                restrict: "EA",
                require: 'ngModel',
                replace: true,
                template: '<div>'
                + '<div layout="row" layout-sm="column" layout-align="center">'
                        + '<md-button class="md-primary" ng-click="showListBottomSheet($event)"> {{ btnText }} </md-button>'
                +'</div>'
                + '<br/>'
                + '<b layout="row" layout-align="center center" layout-margin>{{alert}}</b>'
                + '</div>',
                scope : {
                    dataArray : '=dataArray',
                    type : '=type'
                },
                link: function (scope, element, attrs) {
                    scope.btnText = attrs.btnText;
                },
                controller: function ($scope) {
                    $scope.alert = '';
                    var icons = JSON.parse($scope.dataArray);

                    if($scope.type.toLowerCase() === "grid") {

                        $scope.showGridBottomSheet = function ($event) {
                            $scope.alert = '';
                            $mdBottomSheet.show({
                                template: '<md-bottom-sheet class="md-grid">'
                                + '<md-list>'
                                + '<md-list-item ng-repeat="item in items">'
                                + '<md-button class="md-grid-item-content" ng-click="listItemClick($index)">'
                                + '<md-icon md-svg-src="{{item.icon}}"></md-icon>'
                                + '<div class="md-grid-text"> {{ item.name }} </div>'
                                + '</md-button>'
                                + '</md-list-item>'
                                + '</md-list>'
                                + '</md-bottom-sheet>',
                                controller: function ($scope, $mdBottomSheet) {
                                    $scope.items = icons;
                                    $scope.listItemClick = function ($index) {
                                        var clickedItem = $scope.items[$index];
                                        $mdBottomSheet.hide(clickedItem);
                                    };
                                },
                                targetEvent: $event
                            }).then(function (clickedItem) {
                                $scope.alert = clickedItem.name + ' clicked!';
                            });
                        };

                    }else {

                        $scope.showListBottomSheet = function ($event) {
                            $scope.alert = '';

                            $mdBottomSheet.show({
                                template: ' <md-bottom-sheet class="md-list md-has-header">'
                                + '<md-subheader></md-subheader>'
                                + '<md-list>'
                                + '<md-list-item ng-repeat="item in items">'
                                + '<md-button class="md-list-item-content" ng-click="listItemClick($index)">'
                                + '<md-icon md-svg-src="{{item.icon}}"></md-icon>'
                                + '<span class="md-inline-list-icon-label">{{ item.name }}</span></md-button>'
                                + '</md-list-item>'
                                + '</md-list>'
                                + '</md-bottom-sheet>',
                                controller: function ($scope, $mdBottomSheet) {

                                    $scope.items = icons;

                                    $scope.listItemClick = function ($index) {
                                        var clickedItem = $scope.items[$index];
                                        $mdBottomSheet.hide(clickedItem);
                                    };
                                },
                                targetEvent: $event
                            }).then(function (clickedItem) {
                                $scope.alert = clickedItem.name + ' clicked!';
                            });
                        };
                    }

                }//main-controller

            }//return

        }])
});


