"use strict";

define(['angular'], function (angular) {

    var appServices = angular.module("chartServices", []);

    appServices.factory('ChartService', function () {
        return {

            /**
             * Loads the visualization module from the Google Charts API
             * if available
             * @returns {boolean} - Returns true is successful, or false
             * if not available
             */
            loadGoogleVisualization: function (chart) {

                // Using a try/catch block to guard against unanticipated
                // errors when loading the visualization lib
                try {

                    // Arbitrary callback required in google.load() to
                    // support loading after initial page rendering
                    google.load('visualization', '1', {
                        'callback': 'console.log(\'success\');',
                        'packages': ['corechart']
                    });

                    return true;

                } catch (e) {
                    console.log('Could not load Google lib', e);
                    return false;
                }

            }
        };
    });
});

