"use strict";

define(['angular'], function (angular) {

    var appServices = angular.module("generalServices", []);
    /**
     *  Service para definição de campos multivalorados e concatenação de campos com labelFunction.
     */
    appServices.factory('Util', function () {
        var fn = {};
        fn.formatarCoordenadas = formatarCoordenadas;
        fn.adicionaMenos = adicionaMenos;
        fn.buildIconsObj = buildIconsObj;
        fn.formatarData = formatarData;

        /**
         * @desc Formata coordenadas SEM máscaras
         * @params String coordenada
         */
        function formatarCoordenadas(cord) {
            if (cord != undefined && cord != null && cord.length > 0) {
                cord = cord.replace(/[a-zA-Z.,]+/, "");
                cord = cord.replace(/^([0-9]{2})([0-9]+)$/, "-$1.$2");
            }
            return cord;
        }

        /**
         * @desc Verifica se possui sinal negativo na coordenada, se não, ele adiciona.
         * @params String coordenada
         */
        function adicionaMenos(str) {
            var cord = str.toString();
            if ((cord != undefined && cord != null && cord.length > 0) && cord.search("-") === -1) {
                cord = "-" + cord;
            }
            return cord;
        }

        /**
         * @desc Monta um objeto de icones
         * @params String name,url,size
         * @returns obj
         */
        function buildIconsObj(name,url,size){
            return {
                name : name,
                url : url,
                size : size
            };
        }

        /**
         * Adiciona formatação à um objeto data, com data e hora formatada
         * @param data
         * @returns {{data: string, time: string, dia: number, mes: number, ano: number, horas: number, minutos: number, segundos: number, milisegundos: number, timestamp: number}}
         * @author Vinícius Montanheiro
         */
        function formatarData(data) {
            var dataFormatada = {};

            function formatar(s,l) {
                return (s < l) ? '0' + s : s;
            }

            var novaData = new Date(data);
            var dd = novaData.getDate();
            var mm = novaData.getMonth()+1;
            var yyyy = novaData.getFullYear();
            var h = novaData.getHours();
            var m = novaData.getMinutes();
            var s = novaData.getSeconds();
            var ml = novaData.getMilliseconds();
            return dataFormatada = {
                data : [formatar(dd,10), formatar(mm,10), yyyy].join('/'),
                time : [formatar(h,14),formatar(m,10),formatar(s,10)].join(':'),
                dataEhora : [formatar(dd,10), formatar(mm,10), yyyy].join('/') + " " + [formatar(h,14),formatar(m,10),formatar(s,10)].join(':'),
                dia : dd,
                mes : mm,
                ano : yyyy,
                horas : h,
                minutos : m,
                segundos : s,
                milisegundos : ml,
                timestamp : novaData.getTime()
            }
        }

        return {
            fn: fn
        };

    });
});
