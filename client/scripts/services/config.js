"use strict";
define([
    'angular',
    'scripts/services/generalServices',
    'scripts/services/apiService',
    'scripts/services/socketServices'
], function(angular) {

    var appDirectives = angular.module("appServices",[
        'generalServices',
        'apiService',
        'socketServices'
    ]);

});
