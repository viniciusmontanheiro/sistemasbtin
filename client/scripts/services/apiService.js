"use strict";

define(['angular'], function (angular) {

    var appServices = angular.module("apiService", []);

    appServices.service('ApiService', ['$http', function ($http) {
        this.get = function (url, success, error) {
            return $http.get(url)
                .success(success)
                .error(error);
        };

        this.getBy = function (url, obj, success, error) {
            return $http.get(url, obj)
                .success(success)
                .error(error);
        };

        this.post = function (url, obj, success, error) {
            return $http.post(url, obj)
                .success(success)
                .error(error);
        };

        this.update = function (url, obj, success, error) {
            return $http.put(url, obj)
                .success(success)
                .error(error);
        };

        this.delete = function (url, obj, success, error) {
            return $http.delete(url, obj)
                .success(success)
                .error(error);
        };

    }]);
});
