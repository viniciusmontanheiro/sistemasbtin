"use strict";

define(['angular'], function (angular) {

    var appServices = angular.module("socketServices", []);

    appServices.service('SocketService', ['$http', function ($http) {
        var client;
        this.connect = function connect() {
            client = io.connect('https://localhost:3000');
            return client;
        };

        this.disconnect = function disconnect() {
            if (client) {
                client.disconnect();
            }
        };
    }]);
});
