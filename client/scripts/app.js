'use strict';

define(['angular', 'main', 'aria', 'animate', 'material'], function (angular) {

    var app = angular.module('bTin', ['ui.router', 'ngResource', 'ngAnimate', 'ngAria', 'ngMaterial','appControllers','appDirectives'])

        .config(function ($stateProvider, $urlRouterProvider, $mdThemingProvider, $mdIconProvider) {
            var viewsDir = "../views/";

            $stateProvider.state('index', {
                url: '/',
                templateUrl: viewsDir + "index.html"
            }).state("relatorios", {
                    url: "/relatorios",
                    views: {
                        "container": {
                            templateUrl: viewsDir + "profile/relatorios/pageRelatorios.html"
                        }
                    }
                });

            $mdIconProvider.icon('download', '../../assets/icons/svg/download.svg', 24)
                .icon('print', '../../assets/icons/svg/print.svg', 24)
                .icon('th', '../../assets/icons/svg/th.svg', 24)
                .icon('table', '../../assets/icons/svg/table.svg', 24)
                .icon('columns', '../../assets/icons/svg/columns.svg', 24);

            $mdThemingProvider.definePalette('arduinoColor', {
                '50': '00979D',
                '100': '00979D',
                '200': '00979D',
                '300': '00979D',
                '400': '00979D',
                '500': '00979D',
                '600': '00979D',
                '700': '00979D',
                '800': '00979D',
                '900': '00979D',
                'A100': '00979D',
                'A200': '00979D',
                'A400': '00979D',
                'A700': '00979D',
                'contrastDefaultColor': 'light',
                'contrastDarkColors': ['50', '100', '200', '300', '400', 'A100'],
                'contrastLightColors': undefined
            });
            $mdThemingProvider.theme('default')
                .primaryPalette('arduinoColor');

            $urlRouterProvider.otherwise("/");
        });
    return app;
});

