/**
 * @requires - requireJS
 * @description - Injects dependencies scripts
 * @since - 22/10/2014
 * @author Vinícius Montanheiro
 */

"use strict";

requirejs.config({
    paths: {
        angular: 'assets/components/angular/angular.min',
        main: 'assets/components/main.min',
        animate: 'assets/components/angular-animate/angular-animate.min',
        app: 'scripts/app',
        material: 'assets/components/angular-material/angular-material',
        aria: 'assets/components/angular-aria/angular-aria',
        configService: 'scripts/services/config',
        configController: 'scripts/controllers/config',
        configDirective: 'scripts/directives/config'
    },
    shim: {
        angular: {
            exports: 'angular'
        },
        main: {
            deps: ['angular'],
            exports: 'main'
        },
        app: {
            deps: ['main'],
            exports: 'app'
        },
        aria: {
            deps: ['angular'],
            exports: 'aria'
        },
        material : {
            deps : ['angular','aria','animate'],
            exports : 'material'
        },
        animate : {
            deps : ['angular'],
            exports : 'animate'
        },
        configController : {
            deps: ['angular', 'configDirective', 'configService'],
            exports: 'configController'
        },
        configDirective : {
            deps: ['angular', 'configService'],
            exports: 'configDirective'
        },
        configService: {
            deps: ['angular'],
            exports: 'configService'
        }
    },
    priority: [
        'angular'
    ],
    packages: []
});

window.name = 'NG_DEFER_BOOTSTRAP!';

require([
    'angular',
    'app',
    'configController',
    'configDirective',
    'configService',
    'main',
    'aria',
    'material',
    'animate',
], function (angular,app) {

    //angular.$inject = ['main'];

    /* jshint ignore:start */
    var $html = angular.element(document.getElementsByTagName('html')[0]);
    /* jshint ignore:end */

    angular.element().ready(function () {
        angular.bootstrap(document, [app.name]);
    });

});
