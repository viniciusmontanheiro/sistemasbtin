/**
 * @description Enums
 */

/**
 * @description Dm sim não
 * @param value
 * @param description
 * @constructor
 */
var DmSimNao = function (value, description) {

    this.value = value;
    this.description = description;

    DmSimNao.values.push(this);
};
DmSimNao.values = [];
DmSimNao.NAO = new DmSimNao(0, 'NÃO');
DmSimNao.SIM = new DmSimNao(1, 'SIM');

/**
 * @description Dm tipo pessoa
 * @param value
 * @param description
 * @constructor
 */
var DmTipoPessoa = function (value, description) {

    this.value = value;
    this.description = description;

    DmTipoPessoa.values.push(this);
};
DmTipoPessoa.values = [];
DmTipoPessoa.FISICA = new DmTipoPessoa(1, 'FÍSICA');
DmTipoPessoa.JURIDICA = new DmTipoPessoa(2, 'JURÍDICA');

/**
 * @description Dm tipo venda
 * @param value
 * @param description
 * @constructor
 */
var DmTipoVenda = function (value, description) {

    this.value = value;
    this.description = description;

    DmTipoVenda.values.push(this);
};
DmTipoVenda.values = [];
DmTipoVenda.AVISTA = new DmTipoVenda(0, "À VISTA");
DmTipoVenda.APRASO = new DmTipoVenda(1, "A PRAZO");

/**
 * @description Dm estado civil
 * @param value
 * @param description
 * @constructor
 */
var DmEstadoCivil = function (value, description) {

    this.value = value;
    this.description = description;

    DmEstadoCivil.values.push(this);
};
DmEstadoCivil.values = [];
DmEstadoCivil.SOLTEIRO = new DmEstadoCivil(0, 'SOLTEIRO(a)');
DmEstadoCivil.CASADO = new DmEstadoCivil(1, 'CASADO(a)');
DmEstadoCivil.SEPARADO = new DmEstadoCivil(2, 'SEPARADO(a)');
DmEstadoCivil.DIVORCIADO = new DmEstadoCivil(3, 'DIVORCIADO(a)');
DmEstadoCivil.VIUVO = new DmEstadoCivil(4, 'VIUVO(a)');
DmEstadoCivil.CONCUBINATO = new DmEstadoCivil(5, 'CONCUBINATO(a)');

/**
 * @description Dm sexo
 * @param value
 * @param description
 * @constructor
 */
var DmSexo = function (value, description) {

    this.value = value;
    this.description = description;

    DmSexo.values.push(this);
};
DmSexo.values = [];
DmSexo.MASCULINO = new DmSexo(0, 'MASCULINO');
DmSexo.FEMININO = new DmSexo(1, 'FEMININO');
